/**
 * @file
 * Log page tabbing modifications made by Drupal.TabbingManager.
 */

(($, console) => {
  $(document).on(
    'drupalTabbingContextActivated.devel_a11y',
    (event, tabbingContext) => {
      console.info(
        'TabbingManager: tabbing constraint activated, level %s, %d tabbable elements, %d disabled elements.',
        tabbingContext.level,
        tabbingContext.$tabbableElements.length,
        tabbingContext.$disabledElements.length,
      );
    },
  );

  $(document).on(
    'drupalTabbingContextDeactivated.devel_a11y',
    (event, tabbingContext) => {
      console.info(
        'TabbingManager: tabbing constraint deactivated, level %s.',
        tabbingContext.level,
      );
    },
  );
})(jQuery, window.console);
