/**
 * @file
 * Logs ARIA-live announcements made via Drupal.announce.
 */

((Drupal, console) => {
  const proxied = Drupal.announce;

  Drupal.announce = (text, priority) => {
    const eventualPriority = priority === 'assertive' ? 'assertive' : 'polite';
    console.info('%s Drupal announcement: "%s"', eventualPriority, text);
    return proxied.apply(this, [text, priority]);
  };
})(Drupal, window.console);
