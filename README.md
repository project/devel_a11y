# Devel Accessibility

This module exists to simplify using and testing Drupal's accessibility APIs.

They are currently divided into two broad categories, with specific settings
for each feature that you want assistance for:

1. aural accessibility (screen readers)
    - `Drupal.announce`: can be logged
1. keyboard accessibility
    - `Drupal.TabbingManager`: can be visualized and logged


## Requirements

This module requires the following modules:
- [Devel](https://www.drupal.org/project/devel)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Go to `admin/config/development/devel/a11y` to configure which accessibility
development tools you want to enable.
