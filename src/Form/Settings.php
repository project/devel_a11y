<?php

declare(strict_types=1);

namespace Drupal\devel_a11y\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Devel Accessibility settings for this site.
 */
class Settings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'devel_a11y_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'devel_a11y.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('devel_a11y.settings');
    /** @var bool $config_aural_announce_log */
    $config_aural_announce_log = $config->get('aural.announce.log');
    $form['aural'] = [
      '#type' => 'details',
      '#title' => $this->t('Aural accessibility (screen readers)'),
      '#tree' => TRUE,
      '#open' => TRUE,
    ];
    $form['aural']['announce']['log'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log announcements (ARIA live regions)'),
      '#description' => $this->t('Overrides <code>Drupal.announce()</code> to not only announce UI changes via ARIA live regions, but to also log these aural announcements to the browser console.'),
      '#default_value' => (int) $config_aural_announce_log,
    ];

    $form['keyboard'] = [
      '#type' => 'details',
      '#title' => $this->t('Keyboard accessibility'),
      '#tree' => TRUE,
      '#open' => TRUE,
    ];
    /** @var bool $config_keyboard_tabbingmanager_log */
    $config_keyboard_tabbingmanager_log = $config->get('keyboard.tabbingmanager.log');
    $form['keyboard']['tabbingmanager']['log'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log tabbing manager'),
      '#description' => $this->t('Logs when tabbing is constrained.'),
      '#default_value' => (int) $config_keyboard_tabbingmanager_log,
    ];
    /** @var bool $config_keyboard_tabbingmanager_visualize */
    $config_keyboard_tabbingmanager_visualize = $config->get('keyboard.tabbingmanager.visualize');
    $form['keyboard']['tabbingmanager']['visualize'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Visualize tabbing manager'),
      '#description' => $this->t('Visually indicates which elements are reachable when the tabbing manager is active.'),
      '#default_value' => (int) $config_keyboard_tabbingmanager_visualize,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('devel_a11y.settings')
      ->set('aural.announce.log', (bool) $form_state->getValue([
        'aural',
        'announce',
        'log',
      ]))
      ->set('keyboard.tabbingmanager.log', (bool) $form_state->getValue([
        'keyboard',
        'tabbingmanager',
        'log',
      ]))
      ->set('keyboard.tabbingmanager.visualize', (bool) $form_state->getValue([
        'keyboard',
        'tabbingmanager',
        'visualize',
      ]))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
